import { PipesModule } from "./../pipes/pipes.module";
import { HttpModule } from "@angular/http";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { ErrorHandler, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { Camera } from "@ionic-native/camera";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { IonicStorageModule, Storage } from "@ionic/storage";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { HTTP } from "@ionic-native/http";

import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject
} from "@ionic-native/file-transfer";
import { File } from "@ionic-native/file";

//import { Items } from '../mocks/providers/items';
import { Settings } from "../providers/providers";
import { AuthProvider } from "../providers/providers";
import { StorageProvider } from "./../providers/providers";
import { Api } from "../providers/providers";
import { MyApp } from "./app.component";
import { TvshowProvider } from "../providers/providers";
import { MovieProvider } from "../providers/movie/movie";
import { UserProvider } from "../providers/user/user";
import { CommentProvider } from "../providers/comment/comment";
import { EpisodeProvider } from "../providers/episode/episode";
import { ImageProvider } from "../providers/image/image";
import { ReactiveFormsModule } from "@angular/forms";

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}

export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    option1: "",
    option2: "",
    option3: "",
    option4: "",
    option5: ""
  });
}

@NgModule({
  declarations: [MyApp],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    PipesModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp, { mode: "md" }),
    //IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    ReactiveFormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    Api,
    AuthProvider,
    StorageProvider,
    Camera,
    SplashScreen,
    StatusBar,
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    TvshowProvider,
    MovieProvider,
    UserProvider,
    HTTP,
    CommentProvider,
    EpisodeProvider,
    ImageProvider,
    FileTransfer,
    FileTransferObject,
    File,
    Camera
  ]
})
export class AppModule {}
