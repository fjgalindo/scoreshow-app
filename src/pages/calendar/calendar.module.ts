import { PipesModule } from "./../../pipes/pipes.module";
import { TranslateModule } from "@ngx-translate/core";
import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { CalendarPage } from "./calendar";

@NgModule({
  declarations: [CalendarPage],
  imports: [
    IonicPageModule.forChild(CalendarPage),
    TranslateModule.forChild(),
    PipesModule
  ]
})
export class CalendarPageModule {}
