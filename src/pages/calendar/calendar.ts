import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController
} from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";
import { TvshowProvider } from "../../providers/tvshow/tvshow";
import { UserProvider } from "../../providers/user/user";
import { ImageProvider } from "../../providers/image/image";

/**
 * Generated class for the CalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-calendar",
  templateUrl: "calendar.html"
})
export class CalendarPage {
  premieres: any;
  searchPage: any = "SearchPage";
  loading: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public translateService: TranslateService,
    public tvshowProvider: TvshowProvider,
    public userProvider: UserProvider,
    private loadingCtrl: LoadingController,
    public imageProvider: ImageProvider
  ) {
    this.premieres = null;
  }

  ionViewWillEnter() {
    if (!this.premieres) {
      this.loadPremieres();
    }
  }

  loadPremieres() {
    let loading = this.loadingCtrl.create({
      spinner: "crescent"
    });
    loading.present();
    this.loading = true;

    this.getPremieres().subscribe((res: any) => {
      Object.keys(res).length && (this.premieres = res);

      loading.dismiss();
      this.loading = false;
    });
  }

  getPremieres() {
    return this.userProvider.getPremieres().map(res => res);
  }

  getMovie(movie) {
    this.navCtrl.push("MoviePage", { movie: movie });
  }

  getEpisode(tvshow, episode) {
    this.navCtrl.push("EpisodePage", {
      tvshow: tvshow,
      episode: episode
    });
  }

  doRefresh(refresher) {
    this.getPremieres().subscribe(res => {
      Object.keys(res).length && (this.premieres = res);
      refresher.complete();
    });
  }
}
