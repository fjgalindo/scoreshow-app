import { TvshowProvider } from "./../../providers/tvshow/tvshow";
import { CommentProvider } from "./../../providers/comment/comment";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { ModalController } from "ionic-angular";

import { Tvshow } from "../../models/tvshow";
import { MovieProvider, AuthProvider } from "../../providers/providers";
import { EpisodeProvider } from "../../providers/episode/episode";
import { ImageProvider } from "../../providers/image/image";

/**
 * Generated class for the CommentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-comments",
  templateUrl: "comments.html"
})
export class CommentsPage {
  title: any = null;
  type: string = null;
  episode: any = null;
  comments: any[] = [];
  loading = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private tvshowProvider: TvshowProvider,
    private movieProvider: MovieProvider,
    private episodeProvider: EpisodeProvider,
    private authProvider: AuthProvider,
    public imgProvider: ImageProvider
  ) {}

  ionViewWillEnter() {
    this.title = this.navParams.get("title");
    this.episode = this.navParams.get("episode");
    this.type = this.navParams.get("type");
    this.getComments();
    this.loading = true;
  }

  getComments() {
    let req;
    if (!this.episode) {
      this.type === "tv"
        ? (req = this.tvshowProvider.getComments(this.title._id))
        : (req = this.movieProvider.getComments(this.title._id));
    } else if (this.episode) {
      req = this.episodeProvider.getComments(
        this.title._id,
        this.episode.season_number,
        this.episode.episode_number
      );
    }

    req.subscribe(res => {
      this.comments = res;
      this.loading = false;
    });
  }

  answerComment(comment) {
    let modal = this.modalCtrl.create("AnswerCommentPage", {
      comment: comment
    });

    modal.onDidDismiss(() => {
      this.getComments();
    });

    modal.present();
  }

  addComment(content) {
    let modal = this.modalCtrl.create("NewCommentPage", {
      title: this.title,
      episode: this.episode,
      type: this.type
    });

    modal.onDidDismiss(() => {
      this.getComments();
    });

    modal.present();
  }

  viewUser(user) {
    if (user.id != this.authProvider._user.id) {
      this.navCtrl.push("UserPage", { user });
    }
  }
}
