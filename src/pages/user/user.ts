import { UserProvider } from "./../../providers/user/user";
import { User } from "./../../models/user";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import { PublicUser } from "../../models/publicUser";

/**
 * Generated class for the UserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-user",
  templateUrl: "user.html"
})
export class UserPage {
  user: PublicUser;
  activity: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public userProvider: UserProvider,
    private authProvider: AuthProvider
  ) {
    this.user = this.navParams.get("user");
  }

  ionViewCanEnter() {
    return new Promise((resolve, reject) => {
      if (this.user.id === this.authProvider._user.id) {
        reject();
      } else {
        resolve();
      }
    });
  }

  ionViewDidEnter() {
    this.userProvider.getUserActivity(this.user.id).subscribe(res => {
      this.activity = res;
    });
  }

  followUser(follow) {
    this.userProvider
      .followUser(this.user.id, follow)
      .subscribe(res => (this.user.following = follow));
  }
}
