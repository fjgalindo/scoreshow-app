import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { User } from "../../models";
import { AuthProvider } from "../../providers/auth/auth";
import { CommentProvider } from "../../providers/providers";
import { ImageProvider } from "../../providers/image/image";

/**
 * Generated class for the NewCommentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-new-comment",
  templateUrl: "new-comment.html"
})
export class NewCommentPage {
  title: any;
  episode: any;
  type: string;
  author: User;
  comment: any = {
    content: ""
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private commentProvider: CommentProvider,
    public authProvider: AuthProvider,
    public imgProvider: ImageProvider
  ) {}

  ngOnInit() {
    this.title = this.navParams.get("title");
    this.episode = this.navParams.get("episode");
    this.type = this.navParams.get("type");
    this.author = this.authProvider._user;
  }

  async handleSubmit() {
    if (!this.comment.content) {
      return null;
    }

    if (this.episode) {
      try {
        await this.addEpisodeComment();
        this.viewCtrl.dismiss({ reload: true });
      } catch (err) {
        console.log(err);
      }
    } else {
      try {
        this.addTitleComment();
        this.viewCtrl.dismiss({ reload: true });
      } catch (err) {
        console.log(err);
      }
    }
  }

  async addEpisodeComment() {
    await this.commentProvider.addEpisodeComment(
      this.episode.tvshow_id,
      this.episode.season_number,
      this.episode.episode_number,
      this.comment.content
    );
  }

  async addTitleComment() {
    await this.commentProvider.addTitleComment(
      this.title._id,
      this.type,
      this.comment.content
    );
  }

  close() {
    this.viewCtrl.dismiss({ reload: false }); // Send info if is needed
  }
}
