import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  PopoverController,
  AlertController
} from "ionic-angular";
import { Tvshow } from "../../models/tvshow";
import { EpisodeProvider } from "../../providers/episode/episode";
import { ImageProvider } from "../../providers/image/image";

/**
 * Generated class for the EpisodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-episode",
  templateUrl: "episode.html"
})
export class EpisodePage {
  private tvshow: Tvshow;
  public episode: any;
  public last_comments: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    private alertCtrl: AlertController,
    private episodeProvider: EpisodeProvider,
    public imageProvider: ImageProvider
  ) {
    this.tvshow = this.navParams.get("tvshow");
    this.episode = this.navParams.get("episode");
  }

  ionViewWillEnter() {
    this.loadLastComments();
  }

  roundScore = score => Math.round(score * 10) / 10;

  loadLastComments() {
    this.episodeProvider
      .getLastComments(
        this.tvshow._id,
        this.episode.season_number,
        this.episode.episode_number
      )
      .subscribe((res: any) => res.length && (this.last_comments = res));
  }

  watchEpisode(watched) {
    let tvshowId = this.episode.tvshow_id;
    let seasonN = this.episode.season_number;
    let episodeN = this.episode.episode_number;
    this.episodeProvider
      .watchEpisode(tvshowId, seasonN, episodeN, watched)
      .subscribe(
        res => (this.episode.watched = watched),
        err => console.log("ERROR: ", err)
      );
  }

  viewComments() {
    this.navCtrl.push("CommentsPage", {
      episode: this.episode,
      title: this.tvshow,
      type: "episode"
    });
  }

  showAlert(subTitle) {
    let alert = this.alertCtrl.create({
      title: "Información",
      subTitle,
      buttons: ["Entendido"]
    });
    alert.present();
  }

  showScore(ev) {
    if (!this.episode.watched) {
      return this.showAlert(
        "Debes marcar este episodio como visto antes de calificarlo."
      );
    }
    let popover = this.popoverCtrl.create(
      "ScorePopoverPage",
      {
        score: this.episode.myscore,
        title: this.episode,
        type: "episode"
      },
      { enableBackdropDismiss: false }
    );
    popover.present({
      ev
    });

    popover.onDidDismiss(data => (this.episode.myscore = data.score));
  }
}
