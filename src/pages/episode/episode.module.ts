import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { EpisodePage } from "./episode";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [EpisodePage],
  imports: [IonicPageModule.forChild(EpisodePage), ComponentsModule]
})
export class EpisodePageModule {}
