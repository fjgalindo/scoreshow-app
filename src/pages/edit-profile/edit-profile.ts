import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  App,
  Platform,
  LoadingController,
  ToastController
} from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import { HttpClient } from "@angular/common/http";
import { ImageProvider } from "../../providers/image/image";
import "rxjs/add/operator/first";

import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject
} from "@ionic-native/file-transfer";
import { Camera, CameraOptions } from "@ionic-native/camera";

/**
 * Generated class for the EditProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-edit-profile",
  templateUrl: "edit-profile.html"
})
export class EditProfilePage {
  public user: any = {
    name: "",
    username: "",
    description: "",
    email: "",
    password: "",
    birthdate: "",
    country: ""
  };

  profile_img: any;
  background_img: any;

  imageURI: any;
  imageFileName: any;

  private countries: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private authProvider: AuthProvider,
    private http: HttpClient,
    public app: App,
    public imageProvider: ImageProvider,
    private camera: Camera,
    public platform: Platform,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController
  ) {}

  ionViewDidLoad() {
    this.user = this.authProvider._user;
    this.fetchCountries();
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message,
      duration: 3000,
      position: "top"
    });
    toast.present();
  }

  async setBackgroundImg(e) {
    let loading = this.loadingCtrl.create({ spinner: "crescent" });
    loading.present();
    if (!this.platform.is("cordova")) {
      let fileList: FileList = e.target.files;
      if (fileList.length > 0) {
        let image: File = fileList[0];
        if (this.isImage) {
          await this.authProvider
            .uploadImage("background_img", image)
            .first()
            .toPromise();
        } else {
          this.presentToast("Formato de imagen inválido, debe ser jpeg o png.");
        }
      }
    } else {
      this.handleCordovaUpload().then(
        imageData => (this.background_img = imageData),
        err => {
          console.log(err);
        }
      );
    }
    this.user = await this.authProvider
      .refresh()
      .first()
      .toPromise();
    loading.dismiss();
  }

  async setProfileImg(e) {
    let loading = this.loadingCtrl.create({ spinner: "crescent" });
    loading.present();
    if (!this.platform.is("cordova")) {
      let fileList: FileList = e.target.files;
      if (fileList.length > 0) {
        let image: File = fileList[0];
        if (this.isImage(image)) {
          await this.authProvider.uploadImage("profile_img", image).toPromise();
        } else {
          this.presentToast("Formato de imagen inválido, debe ser jpeg o png.");
        }
      }
    } else {
      this.handleCordovaUpload().then(
        imageData => (this.profile_img = imageData),
        err => {
          console.log(err);
        }
      );
    }
    this.user = await this.authProvider
      .refresh()
      .first()
      .toPromise();
    loading.dismiss();
  }

  isImage(file: File) {
    return file.type === "image/png" || file.type === "image/jpeg";
  }

  handleCordovaUpload() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    };
    return this.camera.getPicture(options);
  }

  fetchCountries() {
    let req = this.http.get("https://restcountries.eu/rest/v2/all");
    req.subscribe(
      (res: any) => {
        this.countries = res;
      },
      err => {
        console.error("ERROR", JSON.stringify(err));
      }
    );
  }

  handleSubmit() {
    this.authProvider.update(this.user);
    if (this.background_img || this.profile_img) {
      let imgs = {
        background_img: this.background_img,
        profile_img: this.profile_img
      };
    }
  }

  updateAccount() {}

  logout() {
    this.authProvider.logout();
    this.app.getRootNav().setRoot("WelcomePage");
  }
}
