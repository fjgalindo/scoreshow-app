import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { IonicPage, NavController, ToastController } from "ionic-angular";

import { AuthProvider } from "../../providers/providers";
//import { MainPage } from '../pages';

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { username: string; password: string } = {
    username: "javi",
    password: "123456789"
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(
    public navCtrl: NavController,
    public user: AuthProvider,
    public toastCtrl: ToastController,
    public translateService: TranslateService
  ) {
    this.translateService.get("LOGIN_ERROR").subscribe(value => {
      this.loginErrorString = value;
    });
  }

  // Attempt to login in through our User service
  doLogin() {
    this.user.login(this.account).subscribe(
      resp => {
        this.navCtrl.setRoot("TabsPage");
      },
      err => {
        let errorMessage = err.error.pop().message || this.loginErrorString;
        let toast = this.toastCtrl.create({
          message: errorMessage,
          duration: 3000,
          position: "top"
        });
        toast.present();
      }
    );
  }
}
