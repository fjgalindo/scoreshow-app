import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { IonicPage, NavController, ToastController } from "ionic-angular";
import {
  FormControl,
  Validators,
  FormGroup,
  AbstractControl,
  ValidatorFn
} from "@angular/forms";

import { AuthProvider } from "../../providers/providers";
import { MainPage } from "../pages";
import { HttpClient } from "@angular/common/http";

@IonicPage()
@Component({
  selector: "page-signup",
  templateUrl: "signup.html"
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account = new FormGroup({
    name: new FormControl("", [Validators.required, Validators.maxLength(60)]),
    username: new FormControl("", [
      Validators.required,
      Validators.minLength(6)
    ]),
    email: new FormControl("", [Validators.required, Validators.email]),
    password: new FormControl("", [
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(40)
      // Validators.pattern(`^[a-zA-Z]$`)
    ]),
    description: new FormControl("", [Validators.maxLength(170)]),
    country: new FormControl("", [Validators.required])
  });

  countries: any[];

  // Our translated text strings
  private signupErrorString: string;

  constructor(
    public navCtrl: NavController,
    public user: AuthProvider,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    private http: HttpClient
  ) {
    this.translateService.get("SIGNUP_ERROR").subscribe(value => {
      this.signupErrorString = value;
    });
    this.fetchCountries();
  }

  allowedContentValidator(nameRe: RegExp): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const allowed = nameRe.test(control.value);
      return allowed ? { allowedContent: { value: control.value } } : null;
    };
  }

  log() {
    console.log(this.account);
  }

  fetchCountries() {
    let req = this.http.get("https://restcountries.eu/rest/v2/all");
    req.subscribe(
      (res: any) => {
        this.countries = res;
      },
      err => {
        console.error("ERROR", JSON.stringify(err));
      }
    );
  }

  doSignup() {
    // Attempt to login in through our User service
    !this.account.invalid &&
      this.user.signup(this.account.value).subscribe(
        resp => {
          this.navCtrl.push(MainPage);
        },
        err => {
          let errorMessage = err.error.message || this.signupErrorString;
          let toast = this.toastCtrl.create({
            message: errorMessage,
            duration: 3000,
            position: "top"
          });
          toast.present();
        }
      );
  }
}
