import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { HttpHeaders } from "@angular/common/http";
import { AuthProvider } from "../../providers/auth/auth";
import { Api } from "../../providers/api/api";
import { UserProvider } from "../../providers/user/user";
import { TvshowProvider, MovieProvider } from "../../providers/providers";
import { PublicUser } from "../../models/publicUser";
import { ImageProvider } from "../../providers/image/image";

@IonicPage()
@Component({
  selector: "page-search",
  templateUrl: "search.html"
})
export class SearchPage {
  userPage: string = "UserPage";
  filter: string = "tmdb";
  activeUser = this.authProvider._user;

  searchKeyword: string;
  results: { users: PublicUser[]; tmdb: any[] } = { users: [], tmdb: [] };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private authProvider: AuthProvider,
    private userProvider: UserProvider,
    private tvshowProvider: TvshowProvider,
    private movieProvider: MovieProvider,
    private api: Api,
    public imageProvider: ImageProvider
  ) {}

  ionViewWillUnload() {
    this.results = {
      users: [],
      tmdb: []
    };
  }

  onInput(e) {
    let keyword = e.target.value;
    if (!keyword) {
      this.results = {
        users: [],
        tmdb: []
      };
      return null;
    }

    if (this.filter === "users") {
      this.searchUsers(keyword);
    } else if (this.filter === "tmdb") {
      this.searchOnTmdb(keyword);
    }
  }

  onCancel(e) {
    this.results = { users: null, tmdb: null };
  }

  getTitle(title) {
    if (title.media_type === "tv") {
      return this.tvshowProvider.getTvshowByTmdbId(title.id);
    } else if (title.media_type === "movie") {
      return this.movieProvider.getMovieByTmdbId(title.id);
    }
  }

  viewTitle(title) {
    this.getTitle(title).subscribe(res => {
      if (title.media_type === "tv") {
        this.navCtrl.push("TvshowPage", { tvshow: res });
      } else {
        this.navCtrl.push("MoviePage", { movie: res });
      }
    });
  }

  searchUsers(keyword) {
    this.userProvider
      .findByName(keyword)
      .subscribe((res: any) => (this.results.users = res));
  }

  applyFilter(category) {
    this.searchKeyword = "";
    this.filter = category;
  }

  searchOnTmdb(keyword) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api.get(`search?query=${keyword}`, {}, { headers }).share();

    seq.subscribe((res: any) => {
      this.results.tmdb = res.results;
    }, err => err);

    return seq;
  }
}
