import { UserProvider } from "./../../providers/user/user";
import { UserActivity } from "./../../models/activity";
import { User } from "./../../models/user";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/providers";
import { TranslateService } from "@ngx-translate/core";

@IonicPage()
@Component({
  selector: "page-profile",
  templateUrl: "profile.html"
})
export class ProfilePage {
  public user: User;
  public activity: UserActivity;
  searchPage: any = "SearchPage";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authProvider: AuthProvider,
    public translateService: TranslateService,
    public userProvider: UserProvider
  ) {}

  ionViewCanEnter() {
    return new Promise((resolve, reject) => {
      this.authProvider.refresh().subscribe((res: any) => {
        const changes = this.user != res;
        this.user = changes && res;
        resolve();
      });
    });
  }

  ionViewWillEnter() {
    this.userProvider
      .getUserActivity(this.user.id)
      .subscribe((res: any) => (this.activity = res));
  }

  goEditProfile() {
    console.log(this.activity);
    this.navCtrl.push("EditProfilePage");
  }
}
