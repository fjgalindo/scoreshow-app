import { ComponentsModule } from "./../../components/components.module";
import { NgModule } from "@angular/core";
import { TranslateModule } from "@ngx-translate/core";
import { IonicPageModule } from "ionic-angular";

import { ProfilePage } from "./profile";

@NgModule({
  declarations: [ProfilePage],
  imports: [
    IonicPageModule.forChild(ProfilePage),
    TranslateModule.forChild(),
    ComponentsModule
  ],
  exports: [ProfilePage]
})
export class ProfilePageModule {}
