import { TitleListComponent } from "./../../components/title-list/title-list";
import { NgModule } from "@angular/core";
import { TranslateModule } from "@ngx-translate/core";
import { IonicPageModule } from "ionic-angular";
import { DiscoverPage } from "./discover";
import { ComponentsModule } from "../../components/components.module";
import { SearchPage } from "../search/search";
import { SearchPageModule } from "../search/search.module";

@NgModule({
  declarations: [DiscoverPage],
  imports: [
    IonicPageModule.forChild(DiscoverPage),
    TranslateModule.forChild(),
    ComponentsModule
  ],
  exports: [DiscoverPage]
})
export class DiscoverPageModule {}
