import { TvshowProvider } from "./../../providers/tvshow/tvshow";
import { Api } from "./../../providers/api/api";
import { AuthProvider } from "./../../providers/auth/auth";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController
} from "ionic-angular";
import { MovieProvider } from "../../providers/movie/movie";
import "rxjs/add/operator/first";

@IonicPage()
@Component({
  selector: "page-discover",
  templateUrl: "discover.html"
})
export class DiscoverPage {
  type: string = "tv";
  searchPage: any = "SearchPage";
  movies: any = {};
  tvshows: any = {};
  loading: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authProvider: AuthProvider,
    public api: Api,
    public tvshowProvider: TvshowProvider,
    public movieProvider: MovieProvider,
    private loadingCtrl: LoadingController
  ) {
    let loading = this.loadingCtrl.create({ spinner: "crescent" });
    loading.present();
    this.updateLists().then(() => {
      loading.dismiss();
    });
  }

  async updateLists() {
    this.loading = true;
    await this.getPopular();
    await this.getTopRated();
    this.loading = false;
  }

  async getPopular() {
    this.tvshows.popular = await this.tvshowProvider
      .getPopular()
      .first()
      .toPromise();
    this.movies.popular = await this.movieProvider
      .getPopular()
      .first()
      .toPromise();
  }

  async getTopRated() {
    this.tvshows.top_rated = await this.tvshowProvider
      .getTopRated()
      .first()
      .toPromise();
    this.movies.top_rated = await this.movieProvider
      .getTopRated()
      .first()
      .toPromise();
  }

  doRefresh(refresher) {
    this.updateLists().then(() => {
      refresher.complete();
    });
  }
}
