import { ComponentsModule } from "./../../components/components.module";
import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { FriendsPage } from "./friends";

@NgModule({
  declarations: [FriendsPage],
  imports: [IonicPageModule.forChild(FriendsPage), ComponentsModule]
})
export class FriendsPageModule {}
