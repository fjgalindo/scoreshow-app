import { UserProvider } from "./../../providers/user/user";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { PublicUser } from "../../models/publicUser";

/**
 * Generated class for the FriendsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-friends",
  templateUrl: "friends.html"
})
export class FriendsPage {
  followeds: PublicUser[] = [];
  searchPage: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public userProvider: UserProvider
  ) {
    this.searchPage = "SearchPage";
  }

  /* ionViewCanEnter() {
    return new Promise((resolve, reject) => {
      this.userProvider.getFolloweds().subscribe(res => {
        this.followeds = res;
        resolve();
      });
    });
  } */

  ionViewWillEnter() {
    this.userProvider.getFolloweds().subscribe((res: any) => {
      console.log(res);
      this.followeds = res;
    });
  }
}
