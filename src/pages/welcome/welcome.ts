import { Component } from "@angular/core";
import { IonicPage, NavController, ToastController } from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";

import { AuthProvider } from "../../providers/providers";
/**
 * The Welcome Page is a splash page that quickly describes the app,
 * and then directs the user to create an account or log in.
 * If you'd like to immediately put the user onto a login/signup page,
 * we recommend not using the Welcome page.
 */
@IonicPage()
@Component({
  selector: "page-welcome",
  templateUrl: "welcome.html"
})
export class WelcomePage {
  // Our translated text strings
  private loginErrorString: string;
  public _showLogin = false;
  constructor(
    public navCtrl: NavController,
    public user: AuthProvider,
    public toastCtrl: ToastController,
    public translateService: TranslateService
  ) {
    this.translateService.get("LOGIN_ERROR").subscribe(value => {
      this.loginErrorString = value;
    });
  }

  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { username: string; password: string } = {
    username: "",
    password: ""
  };

  showLogin(show) {
    this._showLogin = show;
    this.account = {
      username: "",
      password: ""
    };
  }

  // Attempt to login in through our User service
  doLogin() {
    this.user.login(this.account).subscribe(
      resp => {
        this.navCtrl.setRoot("TabsPage");
      },
      err => {
        let errorMessage = err.error.pop().message || this.loginErrorString;
        let toast = this.toastCtrl.create({
          message: errorMessage,
          duration: 3000,
          position: "top"
        });
        toast.present();
      }
    );
  }

  signup() {
    this.navCtrl.push("SignupPage");
  }
}
