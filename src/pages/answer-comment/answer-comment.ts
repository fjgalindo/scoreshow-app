import { CommentProvider, AuthProvider } from "./../../providers/providers";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { User } from "../../models";
import { ImageProvider } from "../../providers/image/image";

/**
 * Generated class for the AnswerCommentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-answer-comment",
  templateUrl: "answer-comment.html"
})
export class AnswerCommentPage {
  comment: any;
  author: User;
  answer: { content: string } = {
    content: ""
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    public commentProvider: CommentProvider,
    private authProvider: AuthProvider,
    public imgProvider: ImageProvider
  ) {}

  ngOnInit() {
    this.comment = this.navParams.get("comment");
    this.author = this.authProvider._user;
  }

  async send() {
    if (this.answer.content) {
      await this.commentProvider.answerComment(
        this.comment.id,
        this.answer.content
      );
      this.dismiss();
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
