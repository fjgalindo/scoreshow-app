import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnswerCommentPage } from './answer-comment';

@NgModule({
  declarations: [
    AnswerCommentPage,
  ],
  imports: [
    IonicPageModule.forChild(AnswerCommentPage),
  ],
})
export class AnswerCommentPageModule {}
