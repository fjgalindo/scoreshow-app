import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { MoviePage } from "./movie";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [MoviePage],
  imports: [IonicPageModule.forChild(MoviePage), ComponentsModule]
})
export class MoviePageModule {}
