import { MovieProvider } from "./../../providers/movie/movie";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController
} from "ionic-angular";
import { AlertController } from "ionic-angular";
import { ImageProvider } from "../../providers/image/image";

/**
 * Generated class for the MoviePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-movie",
  templateUrl: "movie.html"
})
export class MoviePage {
  movie: any;
  last_comments: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public movieProvider: MovieProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public imgProvider: ImageProvider
  ) {
    this.movie = this.navParams.get("movie");
  }

  ionViewWillEnter() {
    this.loadMovie();
    this.loadLastComments();
  }

  loadMovie() {
    this.movieProvider.getFromTMDb(this.movie.id).subscribe((res: any) => {
      this.movie = res;
    });
  }

  loadLastComments() {
    this.movieProvider.getLastComments(this.movie._id).subscribe((res: any) => {
      res.length && (this.last_comments = res);
    });
  }

  viewComments() {
    this.navCtrl.push("CommentsPage", { title: this.movie, type: "movie" });
  }
}
