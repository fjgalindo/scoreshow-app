import { ComponentsModule } from "./../../components/components.module";
import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { TvshowPage } from "./tvshow";

@NgModule({
  declarations: [TvshowPage],
  imports: [IonicPageModule.forChild(TvshowPage), ComponentsModule]
})
export class TvshowPageModule {}
