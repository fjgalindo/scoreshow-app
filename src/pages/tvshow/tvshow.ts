import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { Tvshow } from "../../models/tvshow";
import { TvshowProvider } from "../../providers/tvshow/tvshow";
import { EpisodeProvider } from "../../providers/episode/episode";
import { ImageProvider } from "../../providers/image/image";

/**
 * Generated class for the TvshowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-tvshow",
  templateUrl: "tvshow.html"
})
export class TvshowPage {
  tvshow: Tvshow;
  last_comments: any;
  seasonPage: any = "SeasonPage";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public tvshowProvider: TvshowProvider,
    public episodeProvider: EpisodeProvider,
    public imageProvider: ImageProvider
  ) {
    this.tvshow = this.navParams.get("tvshow");
  }

  ionViewWillEnter() {
    this.loadTvshow();
    this.loadLastComments();
  }

  loadTvshow() {
    this.tvshowProvider.getTvshow(this.tvshow._id).subscribe((res: any) => {
      this.tvshow = res;
      console.log(this.tvshow);
    });
  }

  loadLastComments() {
    this.tvshowProvider
      .getLastComments(this.tvshow._id)
      .subscribe((res: any) => res.length && (this.last_comments = res));
  }

  viewComments() {
    this.navCtrl.push("CommentsPage", { title: this.tvshow, type: "tv" });
  }

  watchSeason(ev, season) {
    ev.target.disabled = true;
    let choice = season.completed;
    season.completed = !choice;
    this.episodeProvider
      .watchSeason(this.tvshow._id, season.season_number, choice)
      .subscribe(res => {
        season.completed = choice;
        this.loadTvshow();
      });
  }
}
