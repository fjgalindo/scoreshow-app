import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { TvshowProvider } from "../../providers/tvshow/tvshow";
import { Tvshow } from "../../models/tvshow";
import { EpisodeProvider } from "../../providers/episode/episode";

/**
 * Generated class for the SeasonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-season",
  templateUrl: "season.html"
})
export class SeasonPage {
  private tvshow: Tvshow;
  season: any;

  constructor(
    public tvshowProvider: TvshowProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public episodeProvider: EpisodeProvider
  ) {
    this.tvshow = this.navParams.get("tvshow");
    this.season = this.navParams.get("season");
  }

  ionViewWillEnter() {
    this.loadSeason();
  }

  loadSeason = () => {
    this.tvshowProvider
      .getSeason(this.tvshow._id, this.season.season_number)
      .subscribe(res => (this.season = res));
  };

  getEpisode(tvshowId, seasonN, episodeN) {
    let episode = this.tvshowProvider
      .getEpisode(tvshowId, seasonN, episodeN)
      .subscribe(res => {
        this.navCtrl.push("EpisodePage", { tvshow: this.tvshow, episode: res });
      });
  }

  watchEpisode(seasonNum, episode) {
    const choice = episode.watched;
    episode.watched = !choice; // Reset checkbox to previous click state.

    const tvshowId = this.tvshow._id;
    this.episodeProvider
      .watchEpisode(tvshowId, seasonNum, episode.episode_number, choice)
      .subscribe(res => (episode.watched = choice));
  }
}
