import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { MovieProvider } from "../../providers/providers";
import { EpisodeProvider } from "../../providers/episode/episode";

/**
 * Generated class for the ScorePopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-score-popover",
  templateUrl: "score-popover.html"
})
export class ScorePopoverPage {
  public score: number;
  public title: any;
  public type: string;

  public loading: boolean = false;
  private saved: boolean = false;

  constructor(
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public movieProvider: MovieProvider,
    public episodeProvider: EpisodeProvider
  ) {
    this.score = this.navParams.get("score");
    this.type = this.navParams.get("type");
    this.title = this.navParams.get("title");
  }

  scoreShow() {
    this.loading = true;
    switch (this.type) {
      case "movie":
        this.movieProvider
          .scoreMovie(this.title._id, this.score)
          .subscribe(res => {
            this.saved = true;
            this.loading = false;
          });
        break;
      case "episode":
        this.episodeProvider
          .scoreEpisode(
            this.title.tvshow_id,
            this.title.season_number,
            this.title.episode_number,
            this.score
          )
          .subscribe(res => {
            this.saved = true;
            this.loading = false;
          });
        break;
      default:
        this.loading = false;
        return null;
    }
  }

  close() {
    this.viewCtrl.dismiss({ score: this.score });
  }
}
