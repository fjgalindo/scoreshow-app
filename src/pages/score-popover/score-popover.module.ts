import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScorePopoverPage } from './score-popover';

@NgModule({
  declarations: [
    ScorePopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(ScorePopoverPage),
  ],
})
export class ScorePopoverPageModule {}
