import { NgModule } from "@angular/core";
import { UserWidgetComponent } from "./user-widget/user-widget";
import { TitleListComponent } from "./title-list/title-list";
import { IonicModule } from "ionic-angular";
import { DiscoverListComponent } from "./discover-list/discover-list";
import { ContactListComponent } from "./contact-list/contact-list";
import { TvshowWidgetComponent } from "./tvshow-widget/tvshow-widget";
import { MovieWidgetComponent } from "./movie-widget/movie-widget";
import { ProfileWidgetComponent } from './profile-widget/profile-widget';
import { CrewListComponent } from './crew-list/crew-list';
import { CastListComponent } from './cast-list/cast-list';

@NgModule({
  declarations: [
    UserWidgetComponent,
    TitleListComponent,
    DiscoverListComponent,
    ContactListComponent,
    TvshowWidgetComponent,
    MovieWidgetComponent,
    ProfileWidgetComponent,
    CrewListComponent,
    CastListComponent
  ],
  imports: [IonicModule],
  exports: [
    UserWidgetComponent,
    TitleListComponent,
    DiscoverListComponent,
    ContactListComponent,
    TvshowWidgetComponent,
    MovieWidgetComponent,
    ProfileWidgetComponent,
    CrewListComponent,
    CastListComponent
  ]
})
export class ComponentsModule {}
