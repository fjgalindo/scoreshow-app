import { Component, Input } from "@angular/core";
import { User } from "../../models";
import { ImageProvider } from "../../providers/image/image";

/**
 * Generated class for the ProfileWidgetComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "profile-widget",
  templateUrl: "profile-widget.html"
})
export class ProfileWidgetComponent {
  @Input() user: User;

  constructor(public imageProvider: ImageProvider) {}

  getImg(img) {
    return this.imageProvider.getImgUrl(img);
  }
}
