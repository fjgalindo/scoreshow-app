import { MovieProvider } from "./../../providers/movie/movie";
import { NavController, LoadingController } from "ionic-angular";
import { TvshowProvider } from "./../../providers/tvshow/tvshow";
import { Component, Input } from "@angular/core";
import "rxjs/add/operator/map";
import { ImageProvider } from "../../providers/image/image";

@Component({
  selector: "title-list",
  templateUrl: "title-list.html"
})
export class TitleListComponent {
  @Input() title: string;
  @Input() type: string;
  @Input() items: any;

  constructor(
    public tvshowProvider: TvshowProvider,
    public movieProvider: MovieProvider,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public imageProvider: ImageProvider
  ) {}

  viewTitle(id) {
    if (this.type === "movie") {
      this.movieProvider.getMovie(id).subscribe(movie => {
        this.navCtrl.push("MoviePage", { movie });
      });
    } else if (this.type === "tv") {
      this.tvshowProvider.getTvshow(id).subscribe(tvshow => {
        this.navCtrl.push("TvshowPage", { tvshow });
      });
    }
  }
}
