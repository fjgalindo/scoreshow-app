import { Component, Input } from "@angular/core";
import { ImageProvider } from "../../providers/image/image";

/**
 * Generated class for the CastListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "cast-list",
  templateUrl: "cast-list.html"
})
export class CastListComponent {
  @Input() src: any[];

  constructor(public imageProvider: ImageProvider) {}

  cast: any[];
  ngOnInit() {
    this.src.length > 0 && (this.cast = this.src.slice(0, 5));
  }
}
