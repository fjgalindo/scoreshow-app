import { Component, Input } from "@angular/core";
import { MovieProvider } from "../../providers/providers";
import { AlertController, PopoverController } from "ionic-angular";
import { ScorePopoverPage } from "../../pages/score-popover/score-popover";
import { ImageProvider } from "../../providers/image/image";

/**
 * Generated class for the MovieWidgetComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "movie-widget",
  templateUrl: "movie-widget.html"
})
export class MovieWidgetComponent {
  @Input() movie: any;
  @Input() type: any;
  score: number;

  constructor(
    private movieProvider: MovieProvider,
    public alertCtrl: AlertController,
    private popoverCtrl: PopoverController,
    public imgProvider: ImageProvider
  ) {}

  watchMovie(watch: boolean) {
    this.movieProvider
      .watchMovie(this.movie._id, watch)
      .subscribe(res => (this.movie.watched = watch), err => console.log(err));
  }

  followMovie(follow: boolean) {
    this.movieProvider
      .followMovie(this.movie._id, follow)
      .subscribe(
        res => (this.movie.following = follow),
        err => console.log(err)
      );
  }

  isReleased() {
    return new Date(this.movie.release_date) < new Date();
  }

  getYear(dateString) {
    return dateString.split("-")[0];
  }

  showAlert(subTitle) {
    let alert = this.alertCtrl.create({
      title: "Información",
      subTitle,
      buttons: ["Entendido"]
    });
    alert.present();
  }

  showScore(ev) {
    if (!this.movie.watched) {
      return this.showAlert(
        "Debes marcar esta película como vista antes de calificarla."
      );
    }
    let popover = this.popoverCtrl.create(
      "ScorePopoverPage",
      {
        score: this.movie.myscore,
        title: this.movie,
        type: "movie"
      },
      { enableBackdropDismiss: false }
    );
    popover.present({
      ev
    });

    popover.onDidDismiss(data => (this.movie.myscore = data.score));
  }

  scoreMovie() {}
}
