import { Input } from "@angular/core";
import { Component } from "@angular/core";
import { TvshowProvider } from "../../providers/tvshow/tvshow";
import { AlertController } from "ionic-angular";
import { ImageProvider } from "../../providers/image/image";

/**
 * Generated class for the TitleWidgetComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "tvshow-widget",
  templateUrl: "tvshow-widget.html"
})
export class TvshowWidgetComponent {
  @Input() tvshow: any;
  @Input() type: any;

  constructor(
    public tvshowProvider: TvshowProvider,
    public alertCtrl: AlertController,
    public imgProvider: ImageProvider
  ) {}

  followTvshow(follow) {
    this.tvshowProvider
      .followTvshow(this.tvshow._id, follow)
      .subscribe(
        res => (this.tvshow.following = follow),
        err => console.log(err)
      );
  }

  /*  getImg(img, tmdb = false) {
    return tmdb ? this.imageProvider.getTmdbImageUrl(img) : "";
  } */

  getYear(dateString) {
    return dateString.split("-")[0];
  }

  getLastAirDate() {
    if (this.tvshow.status === "Returning Series") {
      return "Presente";
    } else {
      return this.getYear(this.tvshow.last_air_date);
    }
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: "Información",
      subTitle:
        "Las puntuaciónes de las series se obtienen a partir de la media de calificaciones de los episodios.",
      buttons: ["Entendido"]
    });
    alert.present();
  }
}
