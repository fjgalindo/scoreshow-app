import { NavController } from "ionic-angular";
import { UserProvider } from "./../../providers/user/user";
import { Component, Input } from "@angular/core";
import { User } from "../../models";
import { ImageProvider } from "../../providers/image/image";

/**
 * Generated class for the ContactListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "contact-list",
  templateUrl: "contact-list.html"
})
export class ContactListComponent {
  @Input() items: User[];

  constructor(
    public userProvider: UserProvider,
    public navCtrl: NavController,
    public imageProvider: ImageProvider
  ) {}

  viewUser(id) {
    this.userProvider.getUser(id).subscribe(res => {
      this.navCtrl.push("UserPage", { user: res });
    });
  }

  getProfileImg(user) {
    return this.imageProvider.getImgUrl(user.profile_img, false);
  }
}
