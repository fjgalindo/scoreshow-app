import { Component, Input } from "@angular/core";
import { User } from "../../models/user";
import { ImageProvider } from "../../providers/image/image";

/**
 * Generated class for the UserWidgetComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "user-widget",
  templateUrl: "user-widget.html"
})
export class UserWidgetComponent {
  @Input() user: User;

  constructor(public imageProvider: ImageProvider) {}

  getImg(img) {
    return this.imageProvider.getImgUrl(img);
  }
}
