import { MovieProvider } from "./../../providers/movie/movie";
import { NavController } from "ionic-angular";
import { TvshowProvider } from "./../../providers/tvshow/tvshow";
import { Component, Input } from "@angular/core";
import "rxjs/add/operator/map";
import { ImageProvider } from "../../providers/image/image";

@Component({
  selector: "discover-list",
  templateUrl: "discover-list.html"
})
export class DiscoverListComponent {
  @Input() type: string;
  @Input() title: string;
  @Input() items: any;

  timeout?: number;

  constructor(
    public tvshowProvider: TvshowProvider,
    public movieProvider: MovieProvider,
    public navCtrl: NavController,
    public imageProvider: ImageProvider
  ) {
    this.title = "Contenidos";
  }

  getTitle(id) {
    /* if (!this.timeout) {
      if (this.type === "movie") {
        this.movieProvider
          .getMovieByTmdbId(id)
          .subscribe(movie => this.navCtrl.push("MoviePage", { movie }));
      } else if (this.type === "tv") {
        this.tvshowProvider
          .getTvshowByTmdbId(id)
          .subscribe(tvshow => this.navCtrl.push("TvshowPage", { tvshow }));
      }
      const clearTimeout = (() => {
        window.clearTimeout(this.timeout);
        this.timeout = null;
      }).bind(this);
      this.timeout = window.setTimeout(() => null, 150);
    } */
    if (this.type === "movie") {
      this.movieProvider
        .getMovieByTmdbId(id)
        .subscribe(movie => this.navCtrl.push("MoviePage", { movie }));
    } else if (this.type === "tv") {
      this.tvshowProvider
        .getTvshowByTmdbId(id)
        .subscribe(tvshow => this.navCtrl.push("TvshowPage", { tvshow }));
    }
  }
}
