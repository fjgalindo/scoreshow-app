import { Component, Input } from "@angular/core";
import { ImageProvider } from "../../providers/image/image";

/**
 * Generated class for the CrewListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "crew-list",
  templateUrl: "crew-list.html"
})
export class CrewListComponent {
  @Input() src: any[];
  crew: any[];

  constructor(public imageProvider: ImageProvider) {}

  ngOnInit() {
    this.src.length > 0 && (this.crew = this.src.slice(0, 5));
  }
}
