export class UserActivity {
  shows: {
    following: {
      tvshows: any[];
      movies: any[];
    };
    watched: {
      movies: any[];
      episodes: any[];
    };
  };
  comments: {
    comments: any[];
    answers: any[];
  };
  follows_users: any[];
}
