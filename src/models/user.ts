export class User {
  id: number = 0;
  username: string = "";
  name: string = "";
  email: string = "";
  password: string = "";
  birthdate: string = "";
  status: number = null;
  auth_key: string = "";
  description: string = "";
  profile_img: string = "";
  background_img: string = "";
  country: string = "";
  tmdb_gtoken: string = "";
}
