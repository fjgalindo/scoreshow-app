export class PublicUser {
  id: number = 0;
  username: string = "";
  name: string = "";
  birthdate: string = "";
  description: string = "";
  profile_img: string = "";
  background_img: string = "";
  country: string = "";
  following: boolean;
}
