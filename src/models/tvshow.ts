export class Tvshow {
  id: number;
  _id: number;
  name: string;
  following: boolean;
  seasons: any[];
  overview: string;
  episode_run_time: any[];
  last_comments: any;
  networks: any[];
  type: string;
}
