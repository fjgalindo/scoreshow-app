import { Api } from "./../api/api";
import { Headers } from "@angular/http";
import { AuthProvider } from "./../auth/auth";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
  headers = new HttpHeaders().set(
    "Authorization",
    `Bearer ${this.authProvider._key}`
  );
  constructor(
    public http: HttpClient,
    public authProvider: AuthProvider,
    public api: Api
  ) {}

  getFolloweds() {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let url = `my/followeds`;
    let req = this.api.get(url, {}, { headers }).share();

    req.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return req;
  }

  getUser(id) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let url = `user/${id}`;
    let req = this.api.get(url, {}, { headers }).share();

    req.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return req;
  }

  getUserActivity(id) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let url = `user/${id}/activity`;
    let req = this.api.get(url, {}, { headers }).share();

    req.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return req;
  }

  getPremieres() {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let url = `my/premieres`;
    let req = this.api.get(url, {}, { headers }).share();

    req.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return req;
  }

  followUser(id, follow) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let url = follow ? `user/${id}/follow` : `user/${id}/unfollow`;
    let req = this.api.post(url, {}, { headers }).share();

    req.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return req;
  }

  findByName(keyword) {
    let url = `user/find?keyword=${keyword}`;
    let req = this.api.get(url, {}, { headers: this.headers }).share();

    req.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return req;
  }

  setProfileImg(formData) {
    let url = `user/upload-image`;
    let req = this.api.post(url, formData, { headers: this.headers }).share();

    req.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return req;
  }
}
