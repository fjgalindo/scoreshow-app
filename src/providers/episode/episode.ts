import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthProvider } from "../auth/auth";
import { Api } from "../api/api";

/*
  Generated class for the EpisodeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EpisodeProvider {
  constructor(
    public http: HttpClient,
    private authProvider: AuthProvider,
    public api: Api
  ) {}

  getComments(tvshowId, seasonNum, episodeNum) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api
      .get(
        `tv/${tvshowId}/season/${seasonNum}/episode/${episodeNum}/comments`,
        {},
        { headers }
      )
      .share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  getLastComments(tvshowId, seasonNum, episodeNum) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api
      .get(
        `tv/${tvshowId}/season/${seasonNum}/episode/${episodeNum}/last-comments`,
        {},
        { headers }
      )
      .share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  watchEpisode(tvshowId, seasonN, episodeN, watch) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let url = watch
      ? `tv/${tvshowId}/season/${seasonN}/episode/${episodeN}/watch`
      : `tv/${tvshowId}/season/${seasonN}/episode/${episodeN}/unwatch`;
    let seq = this.api.post(url, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return seq;
  }

  watchSeason(tvshowId, seasonN, watch) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let url = watch
      ? `tv/${tvshowId}/season/${seasonN}/watch`
      : `tv/${tvshowId}/season/${seasonN}/unwatch`;
    let seq = this.api.post(url, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return seq;
  }

  scoreEpisode(tvshowId, seasonN, episodeN, score) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    const body = new HttpParams().set("score", score);

    let seq = this.api
      .post(
        `tv/${tvshowId}/season/${seasonN}/episode/${episodeN}/score`,
        body,
        { headers }
      )
      .share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }
}
