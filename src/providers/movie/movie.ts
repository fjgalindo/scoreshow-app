import { HttpHeaders, HttpParams } from "@angular/common/http";
import { Headers } from "@angular/http";
import { Injectable } from "@angular/core";

import { Api } from "./../api/api";
import { AuthProvider } from "./../auth/auth";
/*
  Generated class for the MovieProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MovieProvider {
  constructor(public authProvider: AuthProvider, public api: Api) {}

  getMovie(movieId) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api.get(`movie/${movieId}`, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return seq;
  }

  getFromTMDb(id_tmdb) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api.get(`movie/get/${id_tmdb}`, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return seq;
  }

  watchMovie(movieId, watch) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let url = watch ? `movie/${movieId}/watch` : `movie/${movieId}/unwatch`;
    let seq = this.api.post(url, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return seq;
  }

  getPopular() {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api.get(`movie/popular`, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return seq;
  }

  getTopRated() {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api.get(`movie/top-rated`, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return seq;
  }

  getMovieByTmdbId(id_tmdb) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api.get(`movie/get/${id_tmdb}`, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  getLastComments(id) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api
      .get(`movie/${id}/last-comments`, {}, { headers })
      .share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  getComments(id) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api.get(`movie/${id}/comments`, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  followMovie(movieId: number, follow: boolean) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let url = follow ? `movie/${movieId}/follow` : `movie/${movieId}/unfollow`;
    let seq = this.api.post(url, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  scoreMovie(movieId, score) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    const body = new HttpParams().set("score", score);

    let seq = this.api
      .post(`movie/${movieId}/score`, body, { headers })
      .share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }
}
