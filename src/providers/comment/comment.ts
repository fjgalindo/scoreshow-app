import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { AuthProvider } from "./../auth/auth";
import { Api } from "./../api/api";
/*
  Generated class for the CommentProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CommentProvider {
  constructor(
    public http: HttpClient,
    private api: Api,
    private authProvider: AuthProvider
  ) {}

  answerComment(commentId, content) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );
    const body = new HttpParams().set("content", content);

    let url = `comment/${commentId}/answer`;
    let seq = this.api.post(url, body, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return seq;
  }

  getTitleComments(id, type) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api.get(`${type}/${id}/comments`, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  addTitleComment(titleId, type, content) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );
    const body = new HttpParams().set("content", content);
    let seq = this.api
      .post(`${type}/${titleId}/comment`, body, { headers })
      .share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  addEpisodeComment(tvshowId, seasonNum, episodeNum, content) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    const body = new HttpParams().set("content", content);
    let seq = this.api
      .post(
        `tv/${tvshowId}/season/${seasonNum}/episode/${episodeNum}/comment`,
        body,
        { headers }
      )
      .share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }
}
