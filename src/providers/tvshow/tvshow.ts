import { HttpHeaders } from "@angular/common/http";
import { Api } from "./../api/api";
import { AuthProvider } from "./../auth/auth";
import { Injectable } from "@angular/core";

@Injectable()
export class TvshowProvider {
  constructor(public authProvider: AuthProvider, public api: Api) {}

  getTvshow(id) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api.get(`tv/${id}`, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  getSeason(tvshow_id, season_number) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api
      .get(`tv/${tvshow_id}/season/${season_number}`, {}, { headers })
      .share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  getEpisode(tvshowId, seasonN, episodeN) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api
      .get(
        `tv/${tvshowId}/season/${seasonN}/episode/${episodeN}`,
        {},
        { headers }
      )
      .share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return seq;
  }

  followTvshow(tvshowId, follow) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let url = follow ? `tv/${tvshowId}/follow` : `tv/${tvshowId}/unfollow`;
    let seq = this.api.post(url, {}, { headers }).share();

    seq.subscribe((res: any) => res, err => err);
    return seq;
  }

  getPopular() {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let url = `tv/popular`;
    let seq = this.api.get(url, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return seq;
  }

  getTopRated() {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let url = `tv/top-rated`;
    let seq = this.api.get(url, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );
    return seq;
  }

  getTvshowByTmdbId(id_tmdb) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api.get(`tv/get/${id_tmdb}`, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  getPremieres() {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api.get(`tv/premiers`, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  getLastComments(id) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api.get(`tv/${id}/last-comments`, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  getComments(id) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this.authProvider._key}`
    );

    let seq = this.api.get(`tv/${id}/comments`, {}, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }
}
