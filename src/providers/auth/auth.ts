import { HttpHeaders } from "@angular/common/http";
import "rxjs/add/operator/toPromise";
import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";

import { Api } from "../api/api";
import { User } from "./../../models/user";
import { StorageProvider } from "./../storage/storage";

/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }Ø
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class AuthProvider {
  USER_KEY: string = "user";
  AUTH_KEY: string = "auth_key";

  _user: User;
  _key: string;

  constructor(public api: Api, private storageProvider: StorageProvider) {
    // set token if saved in local storage
    /*
    storageProvider.getData("user").then((data:any) => {
      var currentUser = JSON.parse(data.auth_key);
      this._user.auth_key = currentUser && currentUser.auth_key;
    })*/
  }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  login(accountInfo: any) {
    let seq = this.api.post("login", accountInfo).share();

    seq.subscribe(
      (res: any) => {
        this._loggedIn(res.auth_key);
        this.storageProvider.setData(this.AUTH_KEY, res.auth_key);
      },
      err => {
        return err;
      }
    );

    return seq;
  }

  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  signup(accountInfo: any) {
    let seq = this.api.post("register", accountInfo).share();

    seq.subscribe(
      (res: any) => {
        if (res.auth_key) {
          this._loggedIn(res.auth_key);
          this.storageProvider.setData(this.AUTH_KEY, res.auth_key);
        }
      },
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  /**
   * Log the user out, which forgets the session
   */
  logout() {
    this._user = null;
    this.storageProvider.removeData(this.USER_KEY);
    this.storageProvider.removeData(this.AUTH_KEY);
  }

  /**
   * Process a login/signup response to store user data
   */
  _loggedIn(resp) {
    this._key = resp;
  }

  loadActiveUser() {
    return new Promise((resolve, reject) => {
      this.storageProvider.getData(this.USER_KEY).then((result: any) => {
        this._user = JSON.parse(result);
      });
    });
  }

  update(accountInfo) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this._key}`
    );

    let seq = this.api.put("my/update", accountInfo, { headers }).share();

    seq.subscribe(
      (res: any) => {
        this._user = res;
        console.log(this._user);
        this.storageProvider.setData(this.USER_KEY, res);
      },
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  uploadImage(key, image) {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this._key}`
    );
    let formData: FormData = new FormData();
    formData.append(key, image, image.name);
    let seq = this.api.post("my/upload-image", formData, { headers }).share();

    seq.subscribe(
      (res: any) => res,
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }

  refresh() {
    const headers = new HttpHeaders().set(
      "Authorization",
      `Bearer ${this._key}`
    );
    let seq = this.api.get("my", {}, { headers }).share();

    seq.subscribe(
      (res: any) => {
        this._user = res;
        this.storageProvider.setData(this.USER_KEY, res);
        return res;
      },
      err => {
        console.error("ERROR", err);
      }
    );

    return seq;
  }
}
