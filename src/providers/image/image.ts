import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Api } from "../api/api";

/*
  Generated class for the ImageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ImageProvider {
  tmdbUrl: string;
  url: string;
  constructor(public http: HttpClient, private api: Api) {}

  setTmdbImageUrl() {
    let url = `image/tmdb`;
    let seq = this.api.get(url, {}).share();

    seq.subscribe(
      (res: any) => (this.tmdbUrl = res),
      err => {
        console.error("ERROR", err);
      }
    );
  }

  getImgUrl(img, tmdb = false) {
    return tmdb
      ? this.tmdbUrl && this.tmdbUrl + img
      : img && this.api.url + `/image?filename=${img}`;
  }
}
