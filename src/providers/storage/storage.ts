import { Injectable } from "@angular/core";

import { Storage } from "@ionic/storage";
import { Platform } from "ionic-angular";

@Injectable()
export class StorageProvider {
  public data: any = {};

  constructor(private storage: Storage, private platform: Platform) {}

  getData(key) {
    let promesa = new Promise((resolve, reject) => {
      if (this.platform.is("cordova")) {
        // Dispositivo
        this.storage.ready().then(() => {
          let data = this.storage.get(key).then(data => data);
          resolve(data);
        });
      } else {
        // Escritorio
        let data = localStorage.getItem(key);
        resolve(data);
      }
    });
    return promesa;
  }

  setData(key, data) {
    if (this.platform.is("cordova")) {
      // Dispositivo
      this.storage.ready().then(() => {
        this.storage.set(key, data);
      });
    } else {
      // Escritorio
      localStorage.setItem(key, JSON.stringify(data));
    }
  }

  removeData(key) {
    if (this.platform.is("cordova")) {
      // Dispositivo
      this.storage.ready().then(() => {
        this.storage.remove(key);
      });
    } else {
      // Escritorio
      localStorage.removeItem(key);
    }
  }
}
