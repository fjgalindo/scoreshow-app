import { HttpHeaders } from "@angular/common/http";
/* import { Injectable } from "@angular/core";
import { Http, Response, URLSearchParams } from "@angular/http";
import "rxjs/add/operator/map"; */

import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { HTTP } from "@ionic-native/http";

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {
  url: string = "http://localhost:8100/api";
  constructor(public http: HttpClient, public httpNative: HTTP) {}

  /* defaultHeaders = new HttpHeaders({
    "Content-Type": "application/json",
    Authorization: `Bearer ${this.bearerToken}`
  }); */

  get(endpoint: string, params?: any, reqOpts?: any) {
    if (!reqOpts) {
      reqOpts = {
        params: new HttpParams()
      };
    }

    // Support easy query params for GET requests
    if (params) {
      reqOpts.params = new HttpParams();
      for (let k in params) {
        reqOpts.params = reqOpts.params.set(k, params[k]);
      }
    }
    //reqOpts.headers = this.defaultHeaders;

    return this.http.get(this.url + "/" + endpoint, reqOpts);
    //return this.httpNative.get(this.url + "/" + endpoint, {}, reqOpts);
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    return this.http.post(this.url + "/" + endpoint, body, reqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.url + "/" + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http.delete(this.url + "/" + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return this.http.patch(this.url + "/" + endpoint, body, reqOpts);
  }

  /* 
   constructor(public http: Http) {}
  get(endpoint: string, params?: any, reqOpts?: any) {
    if (!reqOpts) {
      reqOpts = {
        params: new URLSearchParams()
      };
    }

    // Support easy query params for GET requests
    if (params) {
      reqOpts.params = new URLSearchParams();
      for (let k in params) {
        reqOpts.params = reqOpts.params.set(k, params[k]);
      }
    }

    return this.http
      .get(this.url + "/" + endpoint, reqOpts)
      .map((response: Response) => {
        return response.json();
      });
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    return this.http
      .post(this.url + "/" + endpoint, body, reqOpts)
      .map((response: Response) => {
        return response.json();
      });
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http
      .put(this.url + "/" + endpoint, body, reqOpts)
      .map((response: Response) => {
        return response.json();
      });
  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http
      .delete(this.url + "/" + endpoint, reqOpts)
      .map((response: Response) => {
        return response.json();
      });
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return this.http
      .patch(this.url + "/" + endpoint, body, reqOpts)
      .map((response: Response) => {
        return response.json();
      });
  } */
}
